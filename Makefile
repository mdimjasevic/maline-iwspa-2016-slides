#!/usr/bin/make -f

PDFLATEX	?= pdflatex -halt-on-error -file-line-error
BIBTEX		?= biber
PDFVIEWER	?= xdg-open

BASENAME 	= slides
PDFTARGET	= $(BASENAME).pdf
SOURCE 		= $(BASENAME).tex

mkfile_path	:= $(abspath $(lastword $(MAKEFILE_LIST)))
current_dir	:= $(notdir $(patsubst %/,%,$(dir $(mkfile_path))))

FIGURES_DIR	= figures
ARCHIVE_NAME	= $(current_dir).tar.gz
ARCHIVE_FILES	= $(SOURCE) $(PDFTARGET) $(FIGURES_DIR) quality-measures.csv Makefile

maline_fig_d	= maline-dynamic.pdf
maline_fig_e	= maline-extraction.pdf
maline_fig_m	= maline-machine-learning.pdf

slides: figures
	$(PDFLATEX) $(BASENAME)
	$(PDFLATEX) $(BASENAME)

view: slides
	$(PDFVIEWER) $(PDFTARGET)

figures: $(FIGURES_DIR)/quality-measures.R quality-measures.csv $(FIGURES_DIR)/malinediagram1.tex $(FIGURES_DIR)/malinediagram2.tex
	(cd $(FIGURES_DIR); R CMD BATCH --no-restore --no-save quality-measures.R)
	(cd $(FIGURES_DIR); $(PDFLATEX) malinediagram1.tex)
	(cd $(FIGURES_DIR); \
	$(PDFLATEX) '\let\ONE=1 \let\ONEH=1 \let\ARROWONE=1 \input' malinediagram2.tex; \
	mv malinediagram2.pdf $(maline_fig_d); \
	$(PDFLATEX) '\let\TWO=1 \let\TWOH=1 \let\ARROWONE=1 \let\ARROWTHREE=1 \input' malinediagram2.tex; \
	mv malinediagram2.pdf $(maline_fig_e); \
	$(PDFLATEX) '\let\THREE=1 \let\THREEH=1 \let\ARROWTHREE=1 \input' malinediagram2.tex; \
	mv malinediagram2.pdf $(maline_fig_m))

archive: slides
	(cd ..; tar cvfz $(ARCHIVE_NAME) $(addprefix $(current_dir)/,$(ARCHIVE_FILES)); mv $(ARCHIVE_NAME) $(current_dir))

clean:
	$(foreach EXT,aux bbl blg log pdf out toc nav snm bcf run.xml,$(RM) $(BASENAME).$(EXT);)
	$(RM) $(ARCHIVE_NAME)
	(cd $(FIGURES_DIR); $(RM) *.aux *.log; $(RM) $(maline_fig_d) $(maline_fig_e) $(maline_fig_m) malinediagram1.pdf quality-measures.Rout quality-measures.pdf)

.PHONY: figures
