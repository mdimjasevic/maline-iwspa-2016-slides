\documentclass[bigger]{beamer}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{fixltx2e}
\usepackage{graphicx}
\usepackage{tikz}
\usetikzlibrary{shapes,calc,arrows,automata,positioning,patterns,chains}
\usepackage[nomessages]{fp}
\usepackage{wrapfig}

% TiKZ Settings
\makeatletter
\tikzset{inside/.code=\preto\tikz@auto@anchor{\pgf@x-\pgf@x\pgf@y-\pgf@y}}
\def\tkzsclfont{0.38}
\FPeval{\sizew}{clip(38*\tkzsclfont)}
\FPeval{\sizeh}{clip(39*\tkzsclfont)}
\newcommand{\little}{\@setfontsize\little{\sizew}{\sizeh}}
% TiKZ Settings

% Default path for figures
\graphicspath{{figures/}}

\beamertemplatenavigationsymbolsempty

\setbeamerfont{page number in head/foot}{}
\setbeamertemplate{footline}[frame number]
\setbeamertemplate{footline}{%
  \raisebox{5pt}{\makebox[\paperwidth]{\hfill\makebox[15pt]{\scriptsize\insertframenumber}}}}

\title{Evaluation of Android Malware Detection Based on System Calls}
\author{{\bf Marko Dimjašević}\inst{1} \and Simone Atzeni\inst{1}\\ \
  \and Ivo Ugrina\inst{2} \and Zvonimir Rakamarić\inst{1}}
\institute{\inst{1} University of Utah, USA \and %
  \inst{2} University of Zagreb, Croatia}
\date{2$^\text{nd}$ IWSPA workshop, March 11, 2016\\New Orleans, LA, USA}

\hypersetup{
  pdfkeywords={security, Android, malware detection, machine learning},
  pdfsubject={Evaluation of Android Malware Detection Based on System Calls}}

\begin{document}

\begingroup
\renewcommand{\insertframenumber}{}
\begin{frame}
  \addtocounter{framenumber}{-1}
  \titlepage
  \vspace{-1cm}
  \begin{center}
  \end{center}
\end{frame}
\endgroup

% See if there are newer figures on malware and if I can have an estimate
% (e.g. 1%) of the malware in Google Play
\section{Motivation and Related Work}
\begin{frame}[label=sec-intro]{Motivation and Related Work}
  \begin{itemize}
  \item Over two billion smartphones in 2016
  \item Android on more than 80\% of smartphones
  \item Permanent presence and development of malicious smartphone apps
  \item About 90\% of malware apps target Android
  \item Conflicting claims in related work
  \end{itemize}
\end{frame}

\section{Our Approach --- maline}
\begin{frame}[label=sec-approach]{Our Approach --- maline}
  \only<1>{
    \begin{itemize}
    \item Dynamic analysis based on app behavior
    \item Random testing of apps in sandbox (system calls)
    \item Binary classification of apps based on feature vectors from system
      calls
    \end{itemize}
  }
  \only<2>{
    \begin{figure}
      \centering
      \includegraphics[width=\textwidth]{maline-tool-flow}
    \end{figure}
  }
\end{frame}

% Fix this figure as the "i" letter is missing in "Files"
\section{maline Tool Flow}
\begin{frame}[label=sec-tool-flow-dyn]{maline Tool Flow --- Dynamic Analysis}
  \begin{center}
    \includegraphics[width=0.96\textwidth]{maline-dynamic}
  \end{center}
\end{frame}

\begin{frame}[label=sec-tool-flow-feature]{maline Tool Flow --- Feature Extraction}
  \begin{center}
    \includegraphics[width=0.96\textwidth]{maline-extraction}
  \end{center}
\end{frame}

\begin{frame}[label=sec-tool-flow-ml]{maline Tool Flow --- Machine Learning}
  \begin{center}
    \includegraphics[width=0.96\textwidth]{maline-machine-learning}
  \end{center}
\end{frame}

\section{Machine Learning}
\begin{frame}[label=sec-ml]{Machine Learning}
  \begin{itemize}
  \item Label assigned to each app
  \item Every app execution represented as feature vector
  \item Goal: learn to categorize feature vectors into two categories
    (goodware and malware)
  \end{itemize}
  \pause
  \begin{block}{Algorithms}
    \begin{itemize}
    \item Support Vector Machines (SVMs)
    \item Random forest (RF)
    \item Lasso
    \item Ridge regression
    \end{itemize}
  \end{block}
\end{frame}

\section{Feature Vector Models}
\begin{frame}[label=sec-vector-models]{Feature Vector Models}
  \begin{enumerate}
  \item Frequency (baseline): count occurrences of each system call
    \begin{itemize}
    \item 360 features
    \end{itemize}
    \pause
  \item Graph: System call dependency
    \begin{itemize}
    \item 129600 features
    \end{itemize}
    \vspace{1cm}
    \begin{figure}[H]
      \begin{centering}
        \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node
          distance=3cm, thick, scale=0.5, every
          node/.style={scale=0.7}, main node/.style={circle,draw}]
          
          \node[state] (0) {$lseek$}; \node[state] (1) [right of=0]
          {$write$}; \node[state] (2) [right of=1] {$time$};
          \node[state] (3) [right of=2] {$ioctl$};
          
          \path[every node/.style={font=\sffamily\small}] (0) edge
          [right] node[right] {} (1) (0) edge [bend left]
          node[right] {} (2) (0) edge [bend left] node[right] {}
          (3) (1) edge [right] node[right] {} (2) (2) edge [right]
          node[right] {} (3);
        \end{tikzpicture}
        % \caption{Modeling Data Flow Between System Calls}
        \label{fig:system}
      \end{centering}
    \end{figure}
  \end{enumerate}
\end{frame}

\section{Empirical Evaluation}
\begin{frame}[label=sec-evaluation]{Empirical Evaluation}
  \begin{block}{Apps}
    \vspace{-2mm}
    \begin{itemize}
    \item Goodware apps: 50k-download apps from Google play (8371 apps)
    \item Malware apps: Drebin data set (4289 apps)
    \end{itemize}
  \end{block}
  \pause
  \vspace{-2mm}
  \begin{block}{Inputs to Apps}
    \vspace{-2mm}
    \begin{itemize}
    \item Pseudo-random internal events: screen clicks, gestures, button presses
    \item External events: SMS messages and location updates
    \end{itemize}
  \end{block}
  \pause
  \vspace{-2mm}
  \begin{block}{Experiments}
    \vspace{-2mm}
    \begin{itemize}
    \item 144 unbalanced- and balanced-design classifiers
    \item 5-fold cross-validation
    \item 9 CPU years
    \end{itemize}
  \end{block}
\end{frame}

\section{Summary of Results}
\begin{frame}[label=sec-results]{Summary of Results}
  \begin{itemize}
  \item Different performance among ML algorithms
  \item Random forest gives best results across 4 quality measures: accuracy,
    sensitivity, specificity, precision
    \begin{itemize}
    \item 94\% accuracy and 97\% specificity
    \end{itemize}
  \item External events bear little information
  \item Number of internal events: no big impact ($\geq 500$)
  \item Structure of feature matrix carries lot of information
  \end{itemize}
\end{frame}

\subsection{Results --- Unbalanced Design, Matrix Sparsity}
\begin{frame}{Results --- Unbalanced Design, Matrix Sparsity}
  \begin{block}{Unbalanced Design}
    \begin{itemize}
    \item Input set: 2 times more goodware than malware
    \item Up- and down-sampling
    \item Sensitivity increased, specificity decreased
    \end{itemize}
  \end{block}
  \begin{block}{Sparsity}
    \begin{itemize}
    \item High sparsity could lead to overfitting
    \item 1000 permutation tests
      \begin{itemize}
      \item Best accuracy: 83\% (the dependency feature model) vs. 93\% for the
        original matrix
      \end{itemize}
    \end{itemize}
  \end{block}
\end{frame}

\subsection{Results --- Quality Measures}
\begin{frame}[label=sec-quality-measures]{Results --- Quality Measures}
  \begin{figure}
    \centering
    \includegraphics[width=\textwidth]{quality-measures}
    % \caption{An experiment with 500 input events with the baseline model and
    %   the unbalanced design.}
    \label{fig:quality}
  \end{figure}
\end{frame}

\subsection{Results --- Random Forest}
\begin{frame}{Results --- Random Forest}
  \begin{figure}
    % \hspace{-7mm}
    \includegraphics[scale=0.45]{roc}
    % \caption{ROC curves created from 5-fold cross-validation with random
    %   forest model.}
    \label{fig:roc}
  \end{figure}
\end{frame}

\subsection{Results --- Random Forest (Up-sampling)}
\begin{frame}{Results --- Random Forest (Up-sampling)}
  \begin{figure}
    % \hspace{-10mm}
    \includegraphics[scale=0.45]{roc-sampling}
    % \caption{ROC curves created from 5-fold cross-validation with random
    %   forest model with up-sampling.}
    \label{fig:roc-upsampling}
  \end{figure}
\end{frame}

\section{Conclusions}
\begin{frame}{Conclusions}
  \vspace{-0.3cm}
  \begin{block}{Approach}
    \begin{itemize}
    \item Dynamic analyses of Android apps
    \item Feature vectors from system calls
    \item Machine learning for classification
    \item Results with simple features close to results with heavyweight
      features
    \end{itemize}
  \end{block}

  \pause

  \begin{wrapfigure}{o}{0.18\textwidth}
    \vspace{-20pt}
    \includegraphics[width=0.16\textwidth]{maline-logo}
    \vspace{-20pt}
  \end{wrapfigure}

  \begin{block}{Tool}
    \begin{itemize}
    \item  \url{https://github.com/soarlab/maline}
    \end{itemize}
  \end{block}
  \vspace{0.8cm}
  \copyright 2016 Marko Dimjašević, CC-BY-SA 4.0
\end{frame}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
